﻿#include "pch.h"
#include <iostream>
#include <cstdlib>
#include <conio.h>
#include <iomanip>

using namespace std;

int main() {

	int n;  

	cout << "Podaj liczbe n\n";
	cin >> n;

	int ** tab = new int *[n];
	
	int i = 0;
	while (i <= n) {

		tab[i] = new int[n];
		i++;
	}
	
	int j = 1;
	while (j <= n) {

		tab[j][0] = j;

		int k = 1;
		while (k < tab[j][0]) {

			tab[j][k] = NULL;

			if (tab[j][0] % k == 0) {
				tab[j][k] = k;
			}

			k++;
		}

		j++;
	}
	
	int l = 0;
	while (l <= n){
		cout << endl;
		
		
		int m = 0;
		while(m < tab[l][0]){
			if (tab[l][m] != 0) cout << setw(n / 10 + 2) << tab[l][m] << " ";
			m++;
		}
		
		l++;
	}
}