#include <iostream>
#include <iomanip>

using namespace std;

int main(){

    int n;

    cout << "Podaj rozmiar tabliczki" << endl;
    cin >> n;

    int ** tab = new int*[n];

    int i = 0;
    while (i < n){

        tab[i] = new int[n];
        i++;
    }

    cout << endl;
    int k = 0;
    while(k < n){

        int j = 0;
        while (j < n){

            tab[k][j] = (k + 1)*(j + 1);
            cout << setw((n / 10) * 2 + 3) << tab[k][j] << " ";
            j++;
        }
        k++;
      cout << endl;
    }

    return 0;
}
