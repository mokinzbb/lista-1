﻿#include "pch.h"
#include <iostream>
#include<iomanip>
#include <windows.h>


using namespace std;


enum wariacje {KOLKO = 'O', KRZYZYK = 'X', PUSTKA = '-'};
char pole[3][3] = {{PUSTKA, PUSTKA, PUSTKA},{PUSTKA, PUSTKA, PUSTKA},{PUSTKA, PUSTKA, PUSTKA}};
int k = 0, n = 0;

void show(){

	int i = 0;
	while (i < 3){

		int j = 0;
		while(j < 3){

			cout << pole[i][j] << " ";
			j++;
		}
		
		i++;
		cout << endl;

	}

	cout << endl;
}

void wstaw(char a) {

	int x, y;

	cout << "Podaj wspolrzedne pola [x,y]" << endl << "WIERSZ X: ";
	cin >> x;
	cout << "KOLUMNA Y: ";
	cin >> y;
	cout << endl;


	if(pole[x - 1][y - 1] == PUSTKA){

		pole[x - 1][y - 1] = a; show(); n = n + 1;
	}

	else{

		if(x > 3 || x < 1 || x < 1 || x > 3) {

			cout << "Podaj wspolrzedne od 1 do 3" << endl;
			wstaw(a);
		}

		else{

			cout << "TO POLE JEST ZAJETE. WPISZ PONOWNIE" << endl;
			wstaw(a);
		}
	}


}

void endgame(int x){

	cout << "WYGRYWA GRACZ " << x << "!";
	
}

void kolumny(int x){

	int j = 0;
	while(j < 3){

		int i = 0;
		if(pole[i][j] != PUSTKA && pole[i][j] == pole[i + 1][j] && pole[i][j] == pole[i + 2][j]) {

			endgame(x);
			break;
		}
		
		j++;
	}
}

void wiersze(int x){

	int i = 0;
	while(i < 3){

		int j = 0;

		if(pole[i][j] != PUSTKA && pole[i][j] == pole[i][j + 1] && pole[i][j] == pole[i][j + 2]){

			endgame(x);
			break;
		}
		
		i++;
	}
}
void diagonale(int x){

	if(pole[0][0] != PUSTKA && pole[0][0] == pole[1][1] && pole[0][0] == pole[2][2]) {

		endgame(x);

	}

	if(pole[2][0] != PUSTKA && pole[2][0] == pole[1][1] && pole[2][0] == pole[0][2])
		endgame(x);
}

void sprawdz(int x){

	wiersze(x);
	kolumny(x);
	diagonale(x);
}


int main(){

	int x = 1;
	cout << "GRACZ 1 - KOLKO \nGRACZ 2 - KRZYZYK" << endl;

	do{

		x = (x + 1) % 2;
		switch (x){

		case 0:

			wstaw(KOLKO);
			sprawdz(1);
			break;

		case 1:

			wstaw(KRZYZYK);
			sprawdz(2);
			break;
		}
	}

	while(k != 2 && n < 9);

	if(n == 9)
		cout << " REMIS! ";

	return 0;
}
